/**
 * 
 */
package us.wishcoder.marketplace.client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.marketplace.server.messages.BuyRequest;
import us.wishcoder.marketplace.server.messages.BuyResponse;
import us.wishcoder.marketplace.server.messages.Item;
import us.wishcoder.marketplace.server.messages.ItemSoldResponse;
import us.wishcoder.marketplace.server.messages.SellRequest;
import us.wishcoder.marketplace.server.messages.SellResponse;
import us.wishcoder.marketplace.server.messages.User;


/**
 * @author ajaysingh
 * 
 */
public class ClientInventory {
	protected final Logger LOG = LoggerFactory.getLogger(ClientInventory.class);
	
	private static final ClientInventory INSTANCE = new ClientInventory();
	private Socket socketServer;
	private static final Map<String, Integer> buyItems = new HashMap<String, Integer>();
	private static final Map<String, Integer> sellItems = new HashMap<String, Integer>();
	private boolean loggedOut;

	/**
	 * ClientInventory
	 */
	private ClientInventory() {
		loggedOut = false;
	}

	/**
	 * getInstance
	 * 
	 * @return
	 */
	public static final ClientInventory getInstance() {
		return INSTANCE;
	}

	/**
	 * @return the loggedOut
	 */
	public boolean isLoggedOut() {
		return loggedOut;
	}

	/**
	 * @param loggedOut
	 *            the loggedOut to set
	 */
	public void setLoggedOut(boolean loggedOut) {
		this.loggedOut = loggedOut;
	}

	/**
	 * initBuyBook
	 * 
	 * @param clientSocket
	 */
	public void initBuyBook(Socket socketServer) {
		this.socketServer = socketServer;
	}

	/**
	 * @return the buyitems
	 */
	public Map<String, Integer> getBuyitems() {
		return buyItems;
	}

	/**
	 * @return the sellitems
	 */
	public Map<String, Integer> getSellitems() {
		return sellItems;
	}

	/**
	 * buyPlaced
	 * 
	 * @param itemName
	 * @param itemQty
	 */
	private void buyPlaced(String itemName, int itemQty) {
		Integer existingQty = buyItems.get(itemName);
		if (existingQty == null) {
			buyItems.put(itemName, itemQty);
		} else {
			buyItems.put(itemName, (existingQty + itemQty));
		}
	}

	/**
	 * sellPlaced
	 * 
	 * @param itemName
	 * @param itemQty
	 */
	private void sellPlaced(String itemName, int itemQty) {
		Integer existingQty = sellItems.get(itemName);
		if (existingQty == null) {
			sellItems.put(itemName, itemQty);
		} else {
			sellItems.put(itemName, (existingQty + itemQty));
		}
	}

	/**
	 * sellPlaced
	 * 
	 * @param itemName
	 * @param itemQty
	 */
	private void updateSellItemsQuantity(Item item) {
		Integer existingQty = sellItems.get(item.getItemName());
		if (existingQty != null) {
			sellItems
					.put(item.getItemName(), (existingQty - item.getItemQty()));
		}
	}

	/**
	 * readAuctionProviderResponse
	 * 
	 * @param serverResponseObj
	 * @throws IOException
	 * @throws Exception
	 */
	public void readAuctionProviderResponse(Object serverResponseObj) throws IOException, Exception {
		if (isLoggedOut()) {
			try {
				socketServer.close();
			} catch (Exception e) {
				// ignore
			} finally {
				socketServer = null;
			}
			return;
		}

		if (serverResponseObj instanceof BuyResponse) {
			BuyResponse buyResponse = (BuyResponse) serverResponseObj;
			readBuyResponse(buyResponse);
		} else if (serverResponseObj instanceof SellResponse) {
			SellResponse sellResponse = (SellResponse) serverResponseObj;
			readSellResponse(sellResponse);
		} else if (serverResponseObj instanceof ItemSoldResponse) {
			ItemSoldResponse itemSoldResponse = (ItemSoldResponse) serverResponseObj;
			readItemSoldResponse(itemSoldResponse);
		}
	}

	/**
	 * sendBuyRequest
	 * 
	 * @param input
	 * @throws Exception
	 */
	public void sendBuyRequest(String input) throws Exception {
		User user = ClientSession.getInstance().getUser();

		String[] entries = input.split(",");
		String itemName = entries[1];
		int buyPrice = Integer.parseInt(entries[2]);
		int buyQty = Integer.parseInt(entries[3]);

		BuyRequest buyRequest = new BuyRequest(user.getUserId(), itemName,
				buyPrice, buyQty);

		ObjectOutputStream oos = new ObjectOutputStream(
				socketServer.getOutputStream());
		oos.writeObject(buyRequest);

		LOG.info("Buy request sent: " + buyRequest.toString());
	}

	/**
	 * readBuyResponse
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	private void readBuyResponse(BuyResponse buyResponse) throws IOException,
			Exception {
		if (!buyResponse.isBuyRequestSuccessful()) {
			LOG.error("Buy request failed due to: "
					+ buyResponse.getErrorCode().toString());
		} else {
			buyPlaced(buyResponse.getBuyRequest().getItemName(), buyResponse
					.getBuyRequest().getBuyQty());
			LOG.info("Buy response received from MarketplaceServerApp: "
					+ buyResponse.toString());
			LOG.info(buyResponse.getItemStatus().toString());
			LOG.info(toString());
		}
	}

	/**
	 * sendSellRequest
	 * 
	 * @param input
	 * @throws Exception
	 */
	public void sendSellRequest(String input) throws Exception {
		User user = ClientSession.getInstance().getUser();

		String[] entries = input.split(",");
		String itemName = entries[1];
		int itemMinPrice = Integer.parseInt(entries[2]);
		int itemSellPrice = Integer.parseInt(entries[3]);
		int itemQty = Integer.parseInt(entries[4]);

		SellRequest sellRequest = new SellRequest(user.getUserId(), itemName,
				itemSellPrice, itemQty, itemMinPrice);

		ObjectOutputStream oos = new ObjectOutputStream(
				socketServer.getOutputStream());
		oos.writeObject(sellRequest);

		LOG.info("Sell request sent: " + sellRequest.toString());
	}

	/**
	 * readSellResponse
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	private void readSellResponse(SellResponse sellResponse)
			throws IOException, Exception {
		if (!sellResponse.isRequestSuccessful()) {
			LOG.error("Sell request failed due to: "
					+ sellResponse.getErrorCode().toString());
		} else {
			sellPlaced(sellResponse.getSellRequest().getItemName(),
					sellResponse.getSellRequest().getItemQty());
			LOG.info("Sell response received from MarketplaceServerApp: "
					+ sellResponse.toString());
			LOG.info(sellResponse.getItemStatus().toString());
			LOG.info(toString());
		}
	}

	/**
	 * readItemSoldResponse
	 * 
	 * @param itemSoldResponse
	 * @throws Exception
	 */
	private void readItemSoldResponse(ItemSoldResponse itemSoldResponse)
			throws IOException, Exception {

		updateSellItemsQuantity(itemSoldResponse.getItem());

		LOG.info("Item sold response received from MarketplaceServerApp: "
				+ itemSoldResponse.toString());
		LOG.info(toString());
	}

	/**
	 * getBuyItemQty
	 * 
	 * @param itemName
	 * @return Integer
	 */
	public final Integer getBuyItemQty(String itemName) {
		return buyItems.get(itemName);
	}

	/**
	 * getSellItemQty
	 * 
	 * @param itemName
	 * @return Integer
	 */
	public final Integer getSellItemQty(String itemName) {
		return sellItems.get(itemName);
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();

		sBuffer.append("ClientInventory Buy Status["
				+ ClientSession.getInstance().getUser().getUserId() + "] [\n");

		for (Map.Entry<String, Integer> entry : buyItems.entrySet()) {
			sBuffer.append("	{itemName=" + entry.getKey() + ", itemQty="
					+ entry.getValue() + "}\n");
		}

		sBuffer.append("], Sell Status[");

		sBuffer.append("ClientInventory Sell Status["
				+ ClientSession.getInstance().getUser().getUserId() + "] [\n");

		for (Map.Entry<String, Integer> entry : sellItems.entrySet()) {
			sBuffer.append("	{itemName=" + entry.getKey() + ", itemQty="
					+ entry.getValue() + "}\n");
		}

		sBuffer.append("]\n");
		return sBuffer.toString();
	}

}
