/**
 * 
 */
package us.wishcoder.marketplace.client;

import us.wishcoder.marketplace.server.messages.User;



/**
 * @author ajaysingh
 *
 */
public class ClientSession {

	private static final ClientSession INSTANCE = new ClientSession();
	private User user;
	
	private ClientSession() {
	}

	public static final ClientSession getInstance(){
		return INSTANCE;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
}
