package us.wishcoder.marketplace.client.tests;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;
import us.wishcoder.crossant.annotation.CrossantStateWriter;
import us.wishcoder.crossant.util.ConfigUtil;
import us.wishcoder.marketplace.client.ClientInventory;
import us.wishcoder.marketplace.client.MarketplaceClientApp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestBuySell extends TestCase{
	protected static final Logger LOG = LoggerFactory
			.getLogger(ConfigUtil.class);
	
	private MarketplaceClientApp  marketplaceClientApp;
	private CountDownLatch lock = new CountDownLatch(1);
	
	@Before
	public void setUp() throws Exception {
		// need to supply system properties in VM arguments:
		// -DuserId=, -DuserPwd=
		marketplaceClientApp = new MarketplaceClientApp("localhost", 9090);
		marketplaceClientApp.connect();
	}

	@After
	public void tearDown() throws Exception {
		if(marketplaceClientApp != null){
			marketplaceClientApp.sendLogout();
		}
		marketplaceClientApp = null;
	}
	
	
	@Test
	public void testSellRequest() throws Exception {
		lock.await(15000, TimeUnit.MILLISECONDS);; // to insure login response is received and processed
		
		String itemName = "tv";
		Integer itemMinPrice = 200;
		Integer itemSellPrice = 300;
		Integer itemQty = 25;
		
		ClientInventory clientInventory = ClientInventory.getInstance();
		clientInventory.sendSellRequest("sell," + itemName + "," + itemMinPrice + "," + itemSellPrice + "," + itemQty );
		
		lock.await(10000, TimeUnit.MILLISECONDS); // to insure sell response is received and processed
		assertEquals(itemQty, clientInventory.getSellItemQty(itemName));
	}
	
	@Test
	public void testBuyRequest() throws Exception {
		lock.await(15000, TimeUnit.MILLISECONDS);; // to insure login response is received and processed
		
		String itemName = "tv";
		Integer itemQty = 5;
		Integer itemPrice = 225;
		
		ClientInventory clientInventory = ClientInventory.getInstance();
		clientInventory.sendBuyRequest("buy," + itemName + "," + itemPrice + "," + itemQty);
		
		lock.await(10000, TimeUnit.MILLISECONDS);; // to insure buy response is received and processed
		assertEquals(itemQty, clientInventory.getBuyItemQty(itemName));
	}
	

	@CrossantStateWriter
	public Map<String, Object> exportSellItemsState(){
		Map<String, Object> statesMap = new HashMap<String, Object>();
		
		try{
			ClientInventory clientInventory = ClientInventory.getInstance();
			statesMap.put("clientInventory", clientInventory.getSellitems());
		}catch(Exception e){
			LOG.error("Exception in TestBuySell.exportBuyItemsState: " + e);
		}
		
		return statesMap;
		
	}
	
	

}
