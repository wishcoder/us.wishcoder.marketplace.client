package us.wishcoder.marketplace.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.marketplace.server.messages.LoginRequest;
import us.wishcoder.marketplace.server.messages.LoginResponse;
import us.wishcoder.marketplace.server.messages.LogoutRequest;



public class MarketplaceClientApp {
	protected final Logger LOG = LoggerFactory.getLogger(MarketplaceClientApp.class);
	
	private final String hostname;
	private final int port;

	private ExecutorService executorService;
	Socket clientSocket;

	
	public MarketplaceClientApp(String hostname, int port) {
		this.hostname = hostname;
		this.port = port;

		// create ClientSession
		ClientSession.getInstance();

		// create ClientInventory
		ClientInventory.getInstance();

		executorService = Executors.newFixedThreadPool(1);
	}
	

	/**
	 * connect
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws Exception
	 */
	public void connect() throws UnknownHostException, IOException, Exception {
		LOG.info("Attempting to connect to MarketplaceServerApp @ "
				+ hostname + ":" + port);
		clientSocket = new Socket(hostname, port);
		executorService.submit(new ClientSocketCallable(clientSocket));
		LOG.info("Connection Established on port: " + port);

		// enrich ClientInventory with clientSocket
		ClientInventory.getInstance().initBuyBook(clientSocket);

		// send login
		sendLogin();
	}

	/**
	 * sendLogin
	 * 
	 * @throws Exception
	 */
	private void sendLogin() throws Exception {
		String userId = System.getProperty("userId", "");
		String userPwd = System.getProperty("userPwd", "");

		LoginRequest loginRequest = new LoginRequest(userId, userPwd);
		ObjectOutputStream oos = new ObjectOutputStream(
				clientSocket.getOutputStream());
		oos.writeObject(loginRequest);

		LOG.info("Login request sent: " + loginRequest.toString());
	}

	/**
	 * sendLogout
	 * 
	 * @throws Exception
	 */
	public void sendLogout() throws Exception {
		LogoutRequest logoutRequest = new LogoutRequest(ClientSession
				.getInstance().getUser().getUserId());
		ObjectOutputStream oos = new ObjectOutputStream(
				clientSocket.getOutputStream());
		oos.writeObject(logoutRequest);

		LOG.info("Logout request sent: " + logoutRequest.toString());

		executorService.shutdownNow();
	}

	public void start() throws Exception {
		try {
			// trying to establish connection to the server
			connect();

			Scanner in = new Scanner(System.in);// stdin
			try {
				while (in.hasNext()) {
					String input = in.next();
					if ("exit".equalsIgnoreCase(input)) {
						sendLogout();
						ClientInventory.getInstance().setLoggedOut(true);
						break;
					} else if (input.toLowerCase().trim().startsWith("buy")) {
						ClientInventory.getInstance().sendBuyRequest(input);
					} else if (input.toLowerCase().trim().startsWith("sell")) {
						ClientInventory.getInstance().sendSellRequest(input);
					}

				}
			} finally {
				LOG.info(ClientSession.getInstance().getUser()
						.getUserId()
						+ " logged out");

				in.close();
				in = null;

				try {
					clientSocket.close();
				} catch (Exception e) {
				}
				executorService.shutdownNow();
				System.exit(0);
			}

		} catch (UnknownHostException e) {
			LOG.error("Host unknown. Cannot establish connection");
		} catch (IOException e) {
			LOG.error("Cannot establish connection. MarketplaceServerApp may not be up."
							+ e.getMessage());
		}
	}

	/**
	 * main
	 * 
	 * @param arg
	 * @throws Exception
	 */
	public static void main(String arg[]) throws Exception {
		// Creating a MarketplaceClientApp object
		MarketplaceClientApp marketplaceClientApp = new MarketplaceClientApp("localhost", 9090);
		marketplaceClientApp.start();
	}

	/**
	 * ClientSocketCallable
	 * 
	 * @author ajaysingh
	 * 
	 */
	static class ClientSocketCallable implements Callable<Object> {
		protected final Logger LOG = LoggerFactory.getLogger(ClientSocketCallable.class);
		
		private final Socket clientSocket;
		private ObjectInputStream streamIn = null;

		ClientSocketCallable(Socket clientSocket){
			this.clientSocket = clientSocket;
		}
		
		@Override
		public Object call() throws Exception {
			while (true) {
				try {
					open();
					
					Object serverResponseObj = streamIn.readObject();
					if (serverResponseObj instanceof LoginResponse) {
						LoginResponse loginResponse = (LoginResponse) serverResponseObj;

						// enrich ClientSession with user
						ClientSession.getInstance().setUser(
								loginResponse.getUser());

						LOG.info("Login response received from MarketplaceServerApp: "
										+ loginResponse.toString());

						LOG.info(loginResponse.getItemStatus()
								.toString());
					} else {
						ClientInventory.getInstance().readAuctionProviderResponse(
								serverResponseObj);
					}

				} catch (IOException ioe) {
				}
			}
		}

		/**
		 * open
		 * 
		 * @throws IOException
		 */
		private void open() throws IOException {
			streamIn = new ObjectInputStream(clientSocket.getInputStream());
		}
	}
}